//
//  APLViewController1.m
//  PhotoPicker
//
//  Created by OKAMOTO Suminori on 2014/10/23.
//  Copyright (c) 2014年 Apple Inc. All rights reserved.
//

#import "APLViewController1.h"
#import "APLViewController0.h"

@interface APLViewController1 (){
    NSString *selectedName;
    
}

@end

@implementation APLViewController1
NSArray *list;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // ホームディレクトリを取得
    //    NSString *homeDir = NSHomeDirectory();
    NSString *fileDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    
    // ファイルマネージャを作成
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    list = [fileManager contentsOfDirectoryAtPath:fileDir
                                            error:&error];
    
    
    
    // ファイルやディレクトリの一覧を表示する
//    for (NSString *path in list) {
//      //  NSLog(@"%@", path);
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return (int)list.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    
    
    //    NSString *imgName = [NSString stringWithFormat:@"%@/simg%04d.jpg",
    //                      [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"],(int)(indexPath.row+1)];
    
    NSString *imgName = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),[list objectAtIndex:indexPath.row]];
    
    //NSLog(@"%@", imgName);
    
    //    NSString *imgName = [NSString stringWithFormat:@"simg%04d.JPG", (int)(indexPath.row+1)]
    UIImage *image = [UIImage imageNamed:imgName];
    imageView.image = image;
    
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    APLViewController0 *detailVC = segue.destinationViewController;
    UICollectionViewCell *cell = (UICollectionViewCell *)sender;
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    detailVC.imagePath =  [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),[list objectAtIndex:indexPath.row]];
    
}

- (IBAction)closeMe:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
