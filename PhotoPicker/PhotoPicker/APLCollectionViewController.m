//
//  APLCollectionViewController.m
//  PhotoPicker
//
//  Created by OKAMOTO Suminori on 2014/10/22.
//  Copyright (c) 2014年 Apple Inc. All rights reserved.
//

#import "APLCollectionViewController.h"

@interface APLCollectionViewController ()
- (IBAction)closeMe:(id)sender;

@end

@implementation APLCollectionViewController
NSArray *list;

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    
    // ホームディレクトリを取得
    //    NSString *homeDir = NSHomeDirectory();
    NSString *fileDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    
    // ファイルマネージャを作成
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    list = [fileManager contentsOfDirectoryAtPath:fileDir
                                                     error:&error];
    
    
    
    // ファイルやディレクトリの一覧を表示する
    for (NSString *path in list) {
        NSLog(@"%@", path);
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return (int)list.count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
//    
    // Configure the cell
    
    UICollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];


//    NSString *imgName = [NSString stringWithFormat:@"%@/simg%04d.jpg",
//                      [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"],(int)(indexPath.row+1)];

    NSString *imgName = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),[list objectAtIndex:indexPath.row]];
    
    NSLog(@"%@", imgName);

//    NSString *imgName = [NSString stringWithFormat:@"simg%04d.JPG", (int)(indexPath.row+1)]
    UIImage *image = [UIImage imageNamed:imgName];
    imageView.image = image;
    
//    UILabel *label = (UILabel *)[cell viewWithTag:2];
//    label.text = [NSString stringWithFormat:@"No.%d",(int)(indexPath.row+1)];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (IBAction)closeMe:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
