//
//  MNZViewControllerPwd.h
//  PhotoPicker
//
//  Created by OKAMOTO Suminori on 2014/10/23.
//  Copyright (c) 2014年 Apple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MNZViewControllerPwd : UIViewController
- (IBAction)closeMe:(id)sender;
- (IBAction)savePwd:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fldNewPwd;
@property (weak, nonatomic) IBOutlet UITextField *fldCurPwd;

@property (weak, nonatomic) IBOutlet UITextField *fldConPwd;
@end
