//
//  APLViewController0.m
//  PhotoPicker
//
//  Created by OKAMOTO Suminori on 2014/10/22.
//  Copyright (c) 2014年 Apple Inc. All rights reserved.
//

#import "APLViewController0.h"

@interface APLViewController0 ()
- (IBAction)closeMe:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *aImage;

@end

@implementation APLViewController0

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage *aImage =[UIImage imageNamed:self.imagePath];
    
    
    //横長なら縦長に90度回転
    
    UIImage *rotatedImage;

    if (aImage.size.height<aImage.size.width) {
        rotatedImage = [UIImage imageWithCGImage:aImage.CGImage scale:aImage.scale orientation:UIImageOrientationRight];
    }else{
        rotatedImage = aImage;
        
    }

    // UIImageViewの初期化
    //縦横の補正
//    CGRect rect = CGRectMake(10, [UIApplication sharedApplication].statusBarFrame.size.height,512*rotatedImage.size.width/rotatedImage.size.height, 512);
    CGRect rect = CGRectMake(10, [UIApplication sharedApplication].statusBarFrame.size.height, 288,288*rotatedImage.size.height/rotatedImage.size.width);
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:rect];

    
    
    imageView.image = rotatedImage;
    
    // UIImageViewのインスタンスをビューに追加
    [self.view addSubview:imageView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeMe:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
