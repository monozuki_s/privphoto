//
//  MNZViewControllerPwd.m
//  PhotoPicker
//
//  Created by OKAMOTO Suminori on 2014/10/23.
//  Copyright (c) 2014年 Apple Inc. All rights reserved.
//

#import "MNZViewControllerPwd.h"

@interface MNZViewControllerPwd ()

@end

@implementation MNZViewControllerPwd

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer:tapRecognizer];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeMe:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)savePwd:(id)sender {
    //パスワードの設定
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *aPwd = [ud stringForKey:@"PWD"];
    
    if ([aPwd length]== 0) {
        if ([self.fldNewPwd.text length]==0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"お知らせ" message:@"パスワードを設定してください。" delegate:self cancelButtonTitle:@"確認" otherButtonTitles:nil];
            [alert show];
        }else{
            if ( [self.fldNewPwd.text isEqualToString:self.fldConPwd.text]) {
            [ud setObject:self.fldNewPwd.text forKey:@"PWD"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"お知らせ" message:@"パスワードを変更しました。" delegate:self cancelButtonTitle:@"確認" otherButtonTitles:nil];
            [alert show];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"お知らせ" message:@"同じパスワードを入力してください。" delegate:self cancelButtonTitle:@"確認" otherButtonTitles:nil];
                [alert show];
                
            }
            
        }

    }else{
        if ([aPwd isEqualToString:self.fldCurPwd.text] && [self.fldNewPwd.text isEqualToString:self.fldConPwd.text]) {
            [ud setObject:self.fldNewPwd.text forKey:@"PWD"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"お知らせ" message:@"パスワードを変更しました。" delegate:self cancelButtonTitle:@"確認" otherButtonTitles:nil];
            [alert show];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"お知らせ" message:@"パスワードが違います。" delegate:self cancelButtonTitle:@"確認" otherButtonTitles:nil];
            [alert show];

        }
    }
}
- (IBAction)textFieldDoneEditing:(id)sender {
    
    // キーボードを閉じる
    [sender resignFirstResponder];
    
}

-(void)tap:(UIGestureRecognizer *)gr{
    [self.view endEditing:YES];
}

@end
