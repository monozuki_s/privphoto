//
//  APLViewController1.h
//  PhotoPicker
//
//  Created by OKAMOTO Suminori on 2014/10/23.
//  Copyright (c) 2014年 Apple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APLViewController1 : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property IBOutlet UICollectionView *collectionView;

- (IBAction)closeMe:(id)sender;

@end
